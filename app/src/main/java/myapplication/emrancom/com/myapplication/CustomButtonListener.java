package myapplication.emrancom.com.myapplication;

import java.util.Map;

public interface CustomButtonListener {

    void handleButtonClicked(int position,String id);
    void dataChanged();


}

