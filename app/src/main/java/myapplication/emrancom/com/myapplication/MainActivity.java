package myapplication.emrancom.com.myapplication;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.io.IOException;
import java.util.Calendar;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import androidx.recyclerview.widget.RecyclerView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {



    String dateOfTwoDaysBefore;
    String dateOfToday;
    String dateOfYesterday;
    String dateOfTomorrow;
    int countFromDbUpdate=0;

    boolean isDateUpdated = false;
    boolean needToDissmisDialog = false;
    int numberOfDataRecived = 0;
    boolean needfetchDataFromDataBase = true;
    int numberOfDBReadeer = 0;

    ArrayList<Match> todayMatches = new ArrayList<Match>();
    ArrayList<Match> yesterdatMatches = new ArrayList<Match>();
    ArrayList<Match> tomorrowMatches = new ArrayList<Match>();
    Map<String,Map<String,String>>  tomorrowMatchesOnDataBase = new HashMap<>();
    Map<String,Map<String,String>>  yesterdatMatchesOnDataBase = new HashMap<>();
    Map<String,Map<String,String>> todayMatchesOnDataBase = new HashMap<>();


    Button setDataTodayButton;
    Button setDataTomorrowButton;
    Button setDataYesterdayButton;

    String currDateDay ;
    ProgressDialog dialog ;

    private RecyclerView recyclerView;
    private MatchAdapter mAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        initDateValues();
        setUpRecyclerView(todayMatches);

        setDataTodayButton = findViewById(R.id.today);
        setDataTomorrowButton = findViewById(R.id.tomorrow);
        setDataYesterdayButton = findViewById(R.id.yestrday);

        setDataTodayButton.setBackground(ContextCompat.getDrawable(this, R.drawable.selected_tab_button));
        setDataTodayButton.setTextColor(getResources().getColor(R.color.colorPrimaryText));

        currDateDay = dateOfToday;

        dialog = ProgressDialog.show(this, "","  מייבא נתונים .. אנא המתן מספר שניות ..." , true);


        if (dialog == null){
            dialog = ProgressDialog.show(this, "מייבא נתונים","אנא המתן מספר שניות ..." , true);
        }

        needfetchDataFromDataBase = true;


        final Handler handler = new Handler();

        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Log.d("refreshData","refresh");
                initAdapterAndGetNewData();
                handler.postDelayed(this,50000);
            }
        };

        handler.post(runnable);
        startDialog();

    }

    @Override
    public void onStart(){
        super.onStart();




    }

    private void fetchDataBaseData(String date, final onFetchCompleted onFinsh){

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("0/"  + "newWinner/" +  date);


        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                onFinsh.dataFromDataBase((Map<String,Map<String,String>>)dataSnapshot.getValue());

             }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

                onFinsh.dataFromDataBase(null);

                Log.w("", "Failed to read value.", error.toException());
            }
        });
    }


    public void initDateValues(){

        Calendar cal = Calendar.getInstance();
         cal.setTime(new Date());
        cal.add(Calendar.MONTH,1);
        dateOfToday = cal.get(Calendar.YEAR) + "-" + cal.get(Calendar.MONTH) +"-" + cal.get(Calendar.DAY_OF_MONTH);
        cal.add(Calendar.DAY_OF_YEAR,1);
        dateOfTomorrow = cal.get(Calendar.YEAR) + "-" + cal.get(Calendar.MONTH) +"-" + cal.get(Calendar.DAY_OF_MONTH);
        cal.setTime(new Date());
        cal.add(Calendar.MONTH,1);
        cal.add(Calendar.DAY_OF_YEAR,-1);
        dateOfYesterday = cal.get(Calendar.YEAR) + "-" + cal.get(Calendar.MONTH) +"-" + cal.get(Calendar.DAY_OF_MONTH);
        cal.setTime(new Date());
        cal.add(Calendar.MONTH,1);
        cal.add(Calendar.DAY_OF_YEAR,-2);
        dateOfTwoDaysBefore = cal.get(Calendar.YEAR) + "-" + cal.get(Calendar.MONTH) +"-" + cal.get(Calendar.DAY_OF_MONTH);



    }

    public void startDialog(){

        dialog.show();
        needToDissmisDialog = true;

    }




    public synchronized void onDataRecived(){

       // initAdapterAndGetNewData(currDataPointer);


    }

    //button Listener
    public void changeDataToDisablay(View view){



        if (view.getId() == setDataTodayButton.getId()){
            setDataTodayButton.setBackground(ContextCompat.getDrawable(this, R.drawable.selected_tab_button));
            setDataTomorrowButton.setBackground(ContextCompat.getDrawable(this, R.drawable.buttonframe));
            setDataYesterdayButton.setBackground(ContextCompat.getDrawable(this, R.drawable.buttonframe));
           // currDataPointer = todayMatches;
           // currLocalDb = DBDataToday;
            currDateDay = dateOfToday;

            initAdapterAndGetNewData();

            return;
        }

        if (view.getId() == setDataTomorrowButton.getId()){
            setDataTodayButton.setBackground(ContextCompat.getDrawable(this, R.drawable.buttonframe));
            setDataTomorrowButton.setBackground(ContextCompat.getDrawable(this, R.drawable.selected_tab_button));
            setDataYesterdayButton.setBackground(ContextCompat.getDrawable(this, R.drawable.buttonframe));
         //   currDataPointer = tomorrowMatches;
           // currLocalDb = DBDataTomorrow;
            currDateDay = dateOfTomorrow;

            initAdapterAndGetNewData();
            return;

        }

        if (view.getId() == setDataYesterdayButton.getId()){
            setDataTodayButton.setBackground(ContextCompat.getDrawable(this, R.drawable.buttonframe));
            setDataTomorrowButton.setBackground(ContextCompat.getDrawable(this, R.drawable.buttonframe));
            setDataYesterdayButton.setBackground(ContextCompat.getDrawable(this, R.drawable.greenbutton));
          //  currDataPointer = yesterdatMatches;
           // currLocalDb = DBDateYesterday;
            currDateDay = dateOfYesterday;
            initAdapterAndGetNewData();

            return;
        }

    }




    class getData extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {

            Calendar currentTime = Calendar.getInstance();
             String html_yes;
            String html_tom;
            String html_today;
            // if(hours>=0 && hours<1 )
            html_yes = "https://m.one.co.il/Mobile/Live/Live.aspx?off=-1";
            html_tom = "https://m.one.co.il/Mobile/Live/Live.aspx?off=1";
            html_today = "https://m.one.co.il/Mobile/live/live.aspx";

             //in case data from one.co.il is not updated yet
           if (!getMatchData(html_today,todayMatches,dateOfToday)) {
               dateOfTomorrow = dateOfToday;
               dateOfToday = dateOfYesterday;
               dateOfYesterday = dateOfTwoDaysBefore;

               getMatchData(html_yes,yesterdatMatches,dateOfYesterday);
               getMatchData(html_tom,tomorrowMatches,dateOfTomorrow);
               getMatchData(html_today,todayMatches,dateOfToday);

           }else {
               getMatchData(html_tom,tomorrowMatches,dateOfTomorrow);
               getMatchData(html_yes,yesterdatMatches,dateOfYesterday);
                return true;
           }



            return true;

        }

        @Override
        protected void onPreExecute() {


        }

         @Override
         protected void onPostExecute(Boolean dataChanged) {
            super.onPostExecute(dataChanged);


                if (mAdapter != null){
                    Log.d("winner","onPostExecute");

                    if(needfetchDataFromDataBase){
                        updateDataBase();

                        needfetchDataFromDataBase = false;
                    }else {

                        if (dialog != null ){
                            dialog.dismiss();
                        }

                        mAdapter.notifyDataSetChanged();


                    }



                }

          }
    }


    private void updateDataBase(){


        fetchDataBaseData(dateOfToday, new onFetchCompleted() {
            @Override
            public void dataFromDataBase(Map<String, Map<String, String>> result) {

                Map<String, Map<String, String>> matchWithVotes = new HashMap<>();
                if (result == null || result.isEmpty()){
                    updateDataBase();
                    return;
                }
                if (todayMatches == null || todayMatches.isEmpty()  ) {
                    return;
                }

                todayMatchesOnDataBase = result;

                for (Match currMatch: todayMatches){

                    if (result != null && result.containsKey(currMatch.getKey())) {
                        String Votes = result.get(currMatch.getKey()).get("Votes");
                        Map<String,String> matchData = new HashMap<>();
                        matchData.put("Score",currMatch.getScoreString());


                        matchData.put("Votes" ,  currMatch.getVoteString());
                        matchWithVotes.put(currMatch.getKey(),matchData);
                    }else {

                        Map<String,String> matchData = new HashMap<>();
                        matchData.put("Score",currMatch.getScoreString());
                        matchData.put("Votes" , currMatch.getVoteString());
                        matchWithVotes.put(currMatch.getKey(),matchData);
                    }



                }

                updateDataBase(dateOfToday,matchWithVotes);
                dismissDialog();

            }
        });

        fetchDataBaseData(dateOfTomorrow, new onFetchCompleted() {
            @Override
            public void dataFromDataBase(Map<String, Map<String, String>> result) {
                Map<String, Map<String, String>> matchWithVotes = new HashMap<>();
                if ( tomorrowMatches == null || tomorrowMatches.isEmpty() ) {
                    return;
                }

                tomorrowMatchesOnDataBase = result;
                for (Match currMatch: tomorrowMatches){

                    if (result != null && result.containsKey(currMatch.getKey())) {
                        String Votes = result.get(currMatch.getKey()).get("Votes");
                        Map<String,String> matchData = new HashMap<>();
//                        if (Votes.split(",").length ==  3){
//                            currMatch.lVotes = Integer.valueOf(Votes.split(",")[0]);
//                            currMatch.xVotes = Integer.valueOf(Votes.split(",")[1]);
//                            currMatch.rVotes = Integer.valueOf(Votes.split(",")[2]);
//                        }
                        matchData.put("Score",currMatch.getScoreString());
                        matchData.put("Votes" , currMatch.getVoteString());
                        matchWithVotes.put(currMatch.getKey(),matchData);
                    }else {

                        Map<String,String> matchData = new HashMap<>();
                        matchData.put("Score",currMatch.getScoreString());
                        matchData.put("Votes" , currMatch.getVoteString());
                        matchWithVotes.put(currMatch.getKey(),matchData);
                    }



                }


                updateDataBase(dateOfTomorrow,matchWithVotes);
                dismissDialog();


            }
        });

        fetchDataBaseData(dateOfYesterday, new onFetchCompleted() {
            @Override
            public void dataFromDataBase(Map<String, Map<String, String>> result) {
                Map<String, Map<String, String>> matchWithVotes = new HashMap<>();
                if (  yesterdatMatches == null || yesterdatMatches.isEmpty() ) {
                    return;
                }
                yesterdatMatchesOnDataBase = result;
                for (Match currMatch: yesterdatMatches){
                    if (result != null && result.containsKey(currMatch.getKey())) {
                        String Votes = result.get(currMatch.getKey()).get("Votes");
                        Map<String,String> matchData = new HashMap<>();

                        matchData.put("Score",currMatch.getScoreString());
                        matchData.put("Votes" ,  currMatch.getVoteString());
                        matchWithVotes.put(currMatch.getKey(),matchData);
                    }else {

                        Map<String,String> matchData = new HashMap<>();
                        matchData.put("Score",currMatch.getScoreString());
                        matchData.put("Votes" , currMatch.getVoteString());
                        matchWithVotes.put(currMatch.getKey(),matchData);
                    }



                }

                updateDataBase(dateOfYesterday,matchWithVotes);
                dismissDialog();


            }
        });



    }

    public synchronized void dismissDialog(){

        countFromDbUpdate++;

        if (countFromDbUpdate < 3){
            return;
        }
        countFromDbUpdate = 0;
        if(dialog != null){
            dialog.dismiss();
        }
        if (mAdapter != null){
            mAdapter.notifyDataSetChanged();
        }

    }

    private void updateDataBase(String date,Map<String,Map<String,String>> data){

        if (data.isEmpty() || data == null) {
            return;
        }

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("0/" + "newWinner/" + date);

        myRef.setValue(data);



    }

    public void setUpRecyclerView(ArrayList<Match> data){

        recyclerView = findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }


    public void initAdapterAndGetNewData(){

        fetchData();

        ArrayList<Match> data = new ArrayList<>();
        if (currDateDay.equals(dateOfToday)) {
            data = todayMatches;
        }else if (currDateDay.equals(dateOfTomorrow)){
            data = tomorrowMatches;

        }else if (currDateDay.equals(dateOfYesterday)){
            data = yesterdatMatches;

        }

        if(mAdapter == null){
            mAdapter = new MatchAdapter(data, new CustomButtonListener() {
                @Override
                public void handleButtonClicked(int position, String id) {
                    onNewVote( position,id);
                }

                @Override
                public void dataChanged() {
                    if (mAdapter != null ){
                        mAdapter.notifyDataSetChanged();

                    }
                }
            },this);

            recyclerView.setAdapter(mAdapter);

        }else  {
            mAdapter.setData(data);
            mAdapter.notifyDataSetChanged();
        }


    }


    public void onNewVote(int position, String id){



        ArrayList<Match> data = new ArrayList<>();
        Map<String,Map<String,String>> dataOnDB = new HashMap<>();

        if (currDateDay.equals(dateOfToday)) {
            data = todayMatches;
            dataOnDB = todayMatchesOnDataBase;
        }else if (currDateDay.equals(dateOfTomorrow)){
            data = tomorrowMatches;
            dataOnDB = tomorrowMatchesOnDataBase;

        }else if (currDateDay.equals(dateOfYesterday)){
            data = yesterdatMatches;
            dataOnDB = yesterdatMatchesOnDataBase;
        }

        Match currMatch = data.get(position);

        String Key = currMatch.getKey();

        if (dataOnDB.get(currMatch.getKey()) == null || position < 0 || position >= data.size() || dataOnDB == null || data == null ){
            return;
        }

        if (id.equals("1")){

            currMatch.increaseLvotes();
            dataOnDB.get(currMatch.getKey()).put("Votes",currMatch.getVoteString());
            updateDataBase(currDateDay,dataOnDB);

        }else if (id.equals("x")){
            currMatch.increaseXvotes();
            dataOnDB.get(currMatch.getKey()).put("Votes",currMatch.getVoteString());

            updateDataBase(currDateDay,dataOnDB);

        }else if (id.equals("2")){

            currMatch.increaseRvotes();
            dataOnDB.get(currMatch.getKey()).put("Votes",currMatch.getVoteString());

            updateDataBase(currDateDay,dataOnDB);
        }


        mAdapter.notifyItemChanged(position);
        //mAdapter.notifyDataSetChanged();
    }



    public void refreshDat(int pos){

        if( mAdapter == null ){
            return;
        }

        mAdapter.notifyItemChanged(pos);

    }


    public void fetchData(){

        new getData().execute();

    }


    public boolean getMatchData(String html, ArrayList<Match> appendTo, String date){


        //appendTo.clear();
        Document doc = null;
         try {

            doc = Jsoup.connect(html).timeout(1000000).userAgent(
                     "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0")
                     .followRedirects(true).get();

        } catch (IOException e) {
            e.printStackTrace();
           // initAdapterAndGetNewData();
           return true;
        }

         Elements link = doc.getAllElements();
        for (Element a : link) {

            String name = a.className();

            if (TextUtils.isEmpty(name) || a.getElementsByClass(name).isEmpty()) {
                continue;
            }

            Log.d("date:emran className",name);

            Log.d("date:emran ",a.getElementsByClass(name).text());

            if (a.hasClass("Status")) {
                Element matchname = a.parent();
                String status = matchname.getElementsByClass("Status").text();
                String lName = matchname.getElementsByClass("hname").text();
                String score = matchname.getElementsByClass("score").text();
                if (score.equals("")) {
                    score += "-";
                }
                if(status.contains("'")){
                    status = status.replace("'","דקה ");
                }

                String rName = matchname.getElementsByClass("gname").text();
                Match newMatch = new Match(status, lName, score, rName,date,new CustomButtonListener() {
                    @Override
                    public void handleButtonClicked(int position, String id) {
                        refreshDat(position);
                    }
                    @Override
                    public void dataChanged() {
                        if (mAdapter != null ){
                            mAdapter.notifyDataSetChanged();

                        }
                    }
                 },this);


                if (!appendTo.contains(newMatch)) {
                    newMatch.PrintMatch();
                    appendTo.add(newMatch);

                    newMatch.listenetFromDB();

                }else{
                   int index = getMatchIndex(newMatch,appendTo);
                    if (index > -1){
                       appendTo.get(index).updateData(newMatch);

                    }
                }


            }
        }
        return true;
    }


    public int getMatchIndex(Match newMatch,ArrayList<Match> data){

        for (int i=0 ; i< data.size();i++){

            if(newMatch.equals(data.get(i))){
                return i;
            }
        }
        return -1;
    }




}


