package myapplication.emrancom.com.myapplication;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import android.text.TextUtils;
import android.util.Log;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.HashMap;


import java.util.Map;

public class Match {


    String dateInDB;
    String status;
    String leftName;
    String rightName;
    String score;
    String lScore;
    String rScore;
    int position;
    Integer lVotes;
    Integer xVotes;
    Integer rVotes;
    Context appContext;
    boolean isButtonsExpanded, isVotesExpanded;

    private CustomButtonListener listener;

    Match( String status, String rName, String score, String leftName,String date, CustomButtonListener lister,Context appContext){
        this.status = status;
        this.leftName = leftName.replace(".","");
        this.rightName = rName.replace(".","");
        this.score = score;
        this.lVotes = 0;
        this.rVotes = 0;
        this.xVotes = 0;
        this.listener = lister;
        this.dateInDB = date;
        this.appContext = appContext;

        String[] scores = score.split("-");

        if (scores.length < 2){
            this.lScore = "0";
            this.rScore = "0";
        }else{
            this.lScore = scores[1];
            this.rScore = scores[0];

        }




    }


    @Override
    public boolean equals(Object o){

        Log.d("winnerLogs","calling match equals ");
            if (o == null){
                Log.d("winnerLogs"," match equals return false ");

                return false;

            }
            final Match newMatch = (Match)o;
            if (newMatch.rightName.equals(this.rightName) && newMatch.leftName.equals(this.leftName)) {
                Log.d("winnerLogs"," match equals return true ");

                return true;
        }
        Log.d("winnerLogs"," match equals return false ");

        return false;

    }

    public int getNumOfVotes(){
        return lVotes + rVotes + xVotes;
    }



    public void PrintMatch(){

        Log.d("name- lname",this.leftName);
        Log.d("name- lscore",this.lScore);

        Log.d("name- rname",this.rightName);
        Log.d("name- rscore",this.rScore);


    }

    public void listenetFromDB(){
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("0/" + "newWinner/" + this.dateInDB  +"/"+ this.getKey());
        myRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Map<String,String> result = (Map<String,String> )dataSnapshot.getValue();
                if (result == null){
                    return;
                }
                String[] Votes = result.get("Votes").split(",");
                if (Votes != null && Votes.length == 3){

                    lVotes = Integer.valueOf(Votes[0]);
                    xVotes = Integer.valueOf(Votes[1]);
                    rVotes = Integer.valueOf(Votes[2]);
                    listener.dataChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public String getStatus() {
        return status;
    }

    public String getLeftName() {
        return leftName;
    }

    public String getRightName() {
        return rightName;
    }

    public String getScore() {
        return score;
    }

    public Integer getlScore() {

        String[] scores = score.split("-");

        if (scores.length < 2){
            this.lScore = "0";
            this.rScore = "0";
        }else{
            this.lScore = scores[1].replace(" ","");
            this.rScore = scores[0].replace(" ","");

        }

        return Integer.valueOf(lScore);
    }

    public Integer getrScore() {

        String[] scores = score.split("-");

        if (scores.length < 2){
            this.lScore = "0";
            this.rScore = "0";
        }else{
            this.lScore = scores[1].replace(" ","");
            this.rScore = scores[0].replace(" ","");

        }

        return Integer.valueOf(rScore);
    }

    public Integer getlVotes() {


        return lVotes;
    }

    public Integer getxVotes() {
        return xVotes;
    }

    public Integer getrVotes() {
        return rVotes;
    }

    public void increaseLvotes() {
        if(isUserAlreadyVoted()){
            return;
        }

        this.lVotes += 1;
        updateUserVote("lVote");



    }

    public void increaseXvotes() {
        if(isUserAlreadyVoted()){
            return;
        }
        this.xVotes += 1;
        updateUserVote("xVote");

     }

    public void increaseRvotes() {

        if(isUserAlreadyVoted()){
            return;
        }

        this.rVotes += 1;
        updateUserVote("rVote");


    }

    public void updateData(Match o){

        Log.d("winnerLogs",o.status);
        this.status = o.status;
        this.score = o.score;


    }

    public String getScoreString(){

        return this.leftName + ":" + this.lScore + "-" + this.rightName + ":" + this.rScore;
    }

    public String getVoteString(){

        return this.lVotes + "," + this.xVotes + "," + this.rVotes;
    }
    //0-x 1-L 2-R
    public Integer getResult(){

        if(getlScore() > getrScore()){
            return 1;
        }
        if(getrScore() > getlScore()){
            return  2;
        }
        return 0;

    }

    //0-x 1-L 2-R
    public Integer getVotesResult(){

        if(getxVotes() > getlVotes() && getxVotes() > getrVotes()){
            return 0;
        }

        if(getlVotes() > getxVotes() && getlVotes() > getrVotes()){
            return 1;
        }
        return 2;

    }

    public String getKey(){
        String Key = this.leftName.replace(" ","-") + "-vs-" + this.rightName.replace(" ","-");
        String modKey = Key.replace("\\","-");
        String finalKey = modKey.replace("/","-");

        Log.d("KEy:-" , finalKey);

        return String.valueOf(sortString(finalKey).hashCode());
    }

    private String sortString(String str ){
        char[] chars = str.toCharArray();
        Arrays.sort(chars);
        String sotredStr = new String(chars);
        return  sotredStr;
    }

    public void updateUserVote(String vote) {


        SharedPreferences pref = appContext.getSharedPreferences("matchVoting", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putString(this.leftName + this.rightName, vote);
        editor.commit();

    }
    public boolean isUserAlreadyVoted(){

        if (status.contains("סיום")){
            return true;
        }

        SharedPreferences pref = appContext.getSharedPreferences("matchVoting", Context.MODE_PRIVATE);
        String userVote = pref.getString(this.leftName + this.rightName, null);
        if (userVote == null){
            return false;
        }
        return true;
    }

    public boolean isButtonsExpanded() {
        return isButtonsExpanded;
    }

    public void setButtonsExpanded(boolean buttonsExpanded) {
        isButtonsExpanded = buttonsExpanded;
    }

    public boolean isVotesExpanded() {
        return isVotesExpanded;
    }

    public void setVotesExpanded(boolean votesExpanded) {
        isVotesExpanded = votesExpanded;
    }

    public String getUserVote(){

        return null;

    }

    public boolean doesUserALreadyVote(){

        return false;

    }

    public boolean doesTheMatchEnd(){
        return false;
    }

}
