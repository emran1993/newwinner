package myapplication.emrancom.com.myapplication;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import static android.graphics.Typeface.BOLD;

public class MatchAdapter extends RecyclerView.Adapter<MatchAdapter.MyViewHolder>  {


    private  ArrayList<Match> data;
    private  CustomButtonListener listener;
    private Context appContext;

    private String xVoteStr = "xVote";
    private String LVoteStr = "lVote";
    private String RVoteStr = "rVote";

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView lName,rName,numOfVotes,score,status, leftVoteTV, rightVoteTV, midVoteTV;
        public Button leftButton, midButton, rightButton;
        public LinearLayout buttonsWrapper, titleWrapper, votesWrapper;

         private WeakReference<CustomButtonListener> listenerRef;

        public MyViewHolder(View view) {
            super(view);
            listenerRef = new WeakReference<>(listener);
            lName = (TextView) view.findViewById(R.id.lName);
            rName = (TextView) view.findViewById(R.id.rName);
            score = (TextView) view.findViewById(R.id.Score);
            status = (TextView) view.findViewById(R.id.status);
            numOfVotes =   view.findViewById(R.id.numvoted);
            leftButton =   view.findViewById(R.id.topButton);
            midButton =   view.findViewById(R.id.midButton);
            rightButton =   view.findViewById(R.id.bottomButton);
            buttonsWrapper = view.findViewById(R.id.buttons_wrapper);
            titleWrapper = view.findViewById(R.id.title_wrapper);
            votesWrapper = view.findViewById(R.id.votes_wrapper);

            leftVoteTV = view.findViewById(R.id.left_votes);
            midVoteTV = view.findViewById(R.id.mid_votes);
            rightVoteTV = view.findViewById(R.id.right_votes);
        }
    }


    public MatchAdapter(ArrayList<Match> data,CustomButtonListener lister,Context appContext) {
        this.data = data;
        this.listener = lister;
        this.appContext = appContext;
    }

    public void setData(ArrayList<Match> data) {
        this.data = data;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final Match match = data.get(position);

        match.position = position;

        resetButtons(holder);

        holder.lName.setText(String.valueOf(match.getLeftName()));
        holder.rName.setText(String.valueOf(match.getRightName()));
        holder.status.setText(String.valueOf(match.getStatus()));
        holder.score.setText(String.valueOf(match.getScore()));
        String totalVotes = match.getNumOfVotes()+" המלצות ";
        Spannable spannable = new SpannableString(totalVotes);
        spannable.setSpan(new StyleSpan(BOLD),0,String.valueOf(match.getNumOfVotes()).length(),Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        spannable.setSpan(new ForegroundColorSpan(appContext.getResources().getColor(R.color.colorPrimaryDark)),0,String.valueOf(match.getNumOfVotes()).length(),Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        holder.numOfVotes.setText(spannable);


        holder.leftButton.setText(String.valueOf( " " + match.getLeftName()  ));
        holder.midButton.setText(String.valueOf("תיקו" ));
        holder.rightButton.setText(String.valueOf(" " + match.getRightName() ));

        SharedPreferences pref = appContext.getSharedPreferences("matchVoting", Context.MODE_PRIVATE);
        String userVote = pref.getString(match.leftName + match.rightName, null);

        holder.buttonsWrapper.setVisibility(match.isButtonsExpanded() ? View.VISIBLE : View.GONE);
        holder.titleWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                match.setButtonsExpanded(!match.isButtonsExpanded());
                notifyItemChanged(match.position);
            }
        });

        View.OnClickListener buttonClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                match.setVotesExpanded(true);
                if (v.getId() == R.id.topButton){
                    listener.handleButtonClicked(position,"1");
                }
                if (v.getId() == R.id.midButton){
                    listener.handleButtonClicked(position,"x");
                }
                if (v.getId() == R.id.bottomButton){
                    listener.handleButtonClicked(position,"2");
                }
                //notifyItemChanged(position);
            }
        };
        holder.leftButton.setOnClickListener(buttonClickListener);
        holder.midButton.setOnClickListener(buttonClickListener);
        holder.rightButton.setOnClickListener(buttonClickListener);


        if (userVote != null) {
            if (userVote.equals(xVoteStr)) {
                setMidButtonSelected(holder);
                //setYellowBackground(0, holder);


            } else if (userVote.equals(LVoteStr)) {
                setLeftButtonSelected(holder);
                //setYellowBackground(1, holder);


            } else if (userVote.equals(RVoteStr)) {
                setRightButtonSelected(holder);
                //setYellowBackground(2, holder);

            }
        }

        if(TextUtils.isEmpty(match.status)){
            holder.status.setVisibility(View.GONE);
        }

        if(match.status.equals("סיום")){
            match.setVotesExpanded(true);
            holder.leftVoteTV.setText(getSpannableForVotes(match.getlVotes(),appContext.getResources().getColor(R.color.colorPrimaryText)));
            holder.midVoteTV.setText(getSpannableForVotes(match.getxVotes(),appContext.getResources().getColor(R.color.colorPrimaryText)));
            holder.rightVoteTV.setText(getSpannableForVotes(match.getrVotes(),appContext.getResources().getColor(R.color.colorPrimaryText)));

            //0-x 1-L 2-R
            Integer res = match.getResult();
            Integer votesResult = match.getVotesResult();

            if (res == votesResult){
                setGreenBackground(res,holder);
             }else  {
                setRedBackground(res,holder);
                setYellowBackground(votesResult,holder);
            }
        }

        if(userVote != null){
            match.setVotesExpanded(true);
            holder.leftVoteTV.setText(getSpannableForVotes(match.getlVotes(), appContext.getResources().getColor(R.color.colorPrimaryText)));
            holder.midVoteTV.setText(getSpannableForVotes(match.getxVotes(), appContext.getResources().getColor(R.color.colorPrimaryText)));
            holder.rightVoteTV.setText(getSpannableForVotes(match.getrVotes(),appContext.getResources().getColor(R.color.colorPrimaryText)));
        }
        if (match.getxVotes() == 0 && match.getlVotes() == 0 && match.getrVotes() == 0){
             resetButtons(holder);
        }

        holder.votesWrapper.setVisibility(match.isVotesExpanded() ? View.VISIBLE : View.GONE);

    }

    private Spannable getSpannableForVotes(Integer userVotes, int color) {
        String votes = userVotes + "\nממליצים";
        Spannable spannable = new SpannableString(votes);
        spannable.setSpan(new StyleSpan(BOLD),0,String.valueOf(userVotes).length(),Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        spannable.setSpan(new ForegroundColorSpan(color),0,String.valueOf(userVotes).length(),Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        return spannable;
    }

    private void setGreenBackground(Integer butIndex,MyViewHolder holder){
        //0-x 1-L 2-R
        if(butIndex.equals(0)){
           holder.midButton.setBackgroundDrawable(ContextCompat.getDrawable(appContext, R.drawable.greenbutton));

        }else if (butIndex.equals(1))
            holder.leftButton.setBackgroundDrawable(ContextCompat.getDrawable(appContext, R.drawable.greenbutton));
        else {
            holder.rightButton.setBackgroundDrawable(ContextCompat.getDrawable(appContext, R.drawable.greenbutton));

        }


    }

    private void setYellowBackground(Integer butIndex,MyViewHolder holder){
        if(butIndex.equals(0)){
            holder.midButton.setBackgroundDrawable(ContextCompat.getDrawable(appContext, R.drawable.yellowbutton));

        }else if (butIndex.equals(1)){
            holder.leftButton.setBackgroundDrawable(ContextCompat.getDrawable(appContext, R.drawable.yellowbutton));

        }else {
            holder.rightButton.setBackgroundDrawable(ContextCompat.getDrawable(appContext, R.drawable.yellowbutton));

        }
    }

    private void setRightButtonSelected(MyViewHolder holder){
        holder.rightButton.setBackgroundDrawable(ContextCompat.getDrawable(appContext, R.drawable.yellowbutton));
    }

    private void setLeftButtonSelected(MyViewHolder holder){
        holder.leftButton.setBackgroundDrawable(ContextCompat.getDrawable(appContext, R.drawable.yellowbutton));
    }

    private void setMidButtonSelected(MyViewHolder holder){
        holder.midButton.setBackgroundDrawable(ContextCompat.getDrawable(appContext, R.drawable.yellowbutton));
    }

    private void setRedBackground(Integer butIndex,MyViewHolder holder){
        if(butIndex.equals(0)){
            holder.midButton.setBackgroundDrawable(ContextCompat.getDrawable(appContext, R.drawable.redbutton));

        }else if (butIndex.equals(1)){
            holder.leftButton.setBackgroundDrawable(ContextCompat.getDrawable(appContext, R.drawable.redbutton));

        }else {
            holder.rightButton.setBackgroundDrawable(ContextCompat.getDrawable(appContext, R.drawable.redbutton));

        }

    }

         private void resetButtons(MyViewHolder holder){

            holder.rightButton.setBackground(ContextCompat.getDrawable(appContext, R.drawable.mid_button_frame));
            holder.leftButton.setBackground(ContextCompat.getDrawable(appContext, R.drawable.mid_button_frame));
            holder.midButton.setBackground(ContextCompat.getDrawable(appContext, R.drawable.mid_button_frame));
        }

    @Override
    public int getItemCount() {
        return data.size();
    }






}
